;;;; Copyright (c) 2017, Philipp Matthias Schäfer (philipp.matthias.schaefer@posteo.de)
;;;;
;;;; This file is part of the Clean Standard Out Test Reporter library.
;;;;
;;;; The Clean Standard Out Test Reporter library is free software: you can
;;;; redistribute it and/or modify it under the terms of the GNU General Public
;;;; License as published by the Free Software Foundation, either version 3 of
;;;; the License, or (at your option) any later version.
;;;;
;;;; The Clean Standard Out Test Reporter library is distributed in the hope
;;;; that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
;;;; warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
;;;; GNU General Public License for more details.
;;;;
;;;; You should have received a copy of the GNU General Public License along
;;;; with the Clean Standard Out Test Reporter library. If not, see
;;;; <https://www.gnu.org/licenses/>.

(defpackage :clean-standard-out-test-reporter/source/all
  (:use :common-lisp
        :clean-test))

(in-package :clean-standard-out-test-reporter/source/all)

(defclass %standard-out-reporter ()
  ((%indentation :initform 0
                 :accessor %indentation)
   (%successes :initform 0
               :accessor %successes)
   (%failures :initform 0
              :accessor %failures)
   (%ignored :initform 0
             :accessor %ignored)
   (%errors :initform 0
            :accessor %errors)
   (%compiler-errors :initform 0
                     :accessor %compiler-errors)))

(defun %indent (reporter)
  (dotimes (ignored (%indentation reporter))
    (format t " ")))

(defmacro %with-print-circle (&body body)
  `(let ((*print-circle* t))
     ,@body))

(defparameter *registered* nil)

(unless *registered*
  (register-reporter (lambda () (make-instance '%standard-out-reporter)))
  (setf *registered* t))

(defmethod on-test-case-start ((reporter %standard-out-reporter)
                               test-case test-suite)
  (%with-print-circle
    (%indent reporter)
    (format t "~a: " (test-case-name test-case))))

(defmethod on-test-case-end ((reporter %standard-out-reporter)
                             (result test-case-ignored-result) test-suite)
  (incf (%ignored reporter))
  (%with-print-circle
    (format t "Ignored~%")))

(defmethod on-test-case-end ((reporter %standard-out-reporter)
                             (result test-case-success-result) test-suite)
  (incf (%successes reporter))
  (%with-print-circle
    (format t "Success~%")))

(defmethod on-test-case-end ((reporter %standard-out-reporter)
                             (result test-case-error-result) test-suite)
  (incf (%errors reporter))
  (%with-print-circle
    (format t "Error! (Condition: ~a)~%" (test-case-error-result-error result))))

(defmethod on-test-case-end ((reporter %standard-out-reporter)
                             (result test-case-compilation-error-result) test-suite)
  (incf (%compiler-errors reporter))
  (%with-print-circle
    (format t "Compilation Error!~%")))

(defmethod on-test-case-end ((reporter %standard-out-reporter)
                             (result test-case-failure-result) test-suite)
  (incf (%failures reporter))
  (%with-print-circle
    (format t "Failure! (Failed form: ~a)~%"
            (assert-failure-condition-test-form
             (test-case-failure-result-condition result)))))

(defmethod on-test-suite-start ((reporter %standard-out-reporter) test-suite)
  (%with-print-circle
    (format t "~%")
    (%indent reporter)
    (format t "Run tests for ~a~%" (test-suite-name test-suite)))
  (incf (%indentation reporter) 2))

(defmethod on-test-suite-end ((reporter %standard-out-reporter)
                              test-suite-result)
  (decf (%indentation reporter) 2))

(defmethod on-end ((reporter %standard-out-reporter) results)
  (%with-print-circle
    (format t "~%~A Success~:*~[es~;~:;es~], ~
             ~A Failure~:*~[s~;~:;s~], ~
             ~A Ignored Test~:*~[s~;~:;s~], ~
             ~A Error~:*~[s~;~:;s~], ~
             ~A Compiler Error~:*~[s~;~:;s~] ~
             (~A in total)~%"
            (%successes reporter)
            (%failures reporter)
            (%ignored reporter)
            (%errors reporter)
            (%compiler-errors reporter)
            (+ (%successes reporter)
               (%failures reporter)
               (%ignored reporter)
               (%errors reporter)
               (%compiler-errors reporter)))))
