# Clean Test Library

Clean Standard Out Test Reporter for the Clean Test library writes test result
information to `COMMON-LISP:*STANDARD-OUTPUT*`. Note the report functions are
called within the dynamic context of the function running the tests, that is
`CLEAN-TEST:RUN` or `CLEAN-TEST:RUN-TEST-SUITE`.

There is no public interface. The reporter is automatically registered, when the
system is loaded. From then on, it will report on all tests that are run.

## Clean Project

This library is part of the [Clean Project](https://gitlab.com/clean),
the outlet for my NIH syndrome.

## License

This library was written by Philipp Matthias Schäfer
(philipp.matthias.schaefer@posteo.de) and is published under the GPL3 license.
See [LICENSE] for a copy of that license.
