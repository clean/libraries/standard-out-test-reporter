# Change Log

All notable changes to this project are documented in this file. This project
adheres to [Semantic Versioning](https://semver.org)

## [3.0.0] 2017-02-27

### Added

- Print of summary line at the end of each run

### Changed

- Adapted to new API in version 3.0.0 of Clean Test

## [2.0.0] 2017-02-26

### Changed

- Adapted to new API in version 2.0.0 of Clean Test

## [1.0.0] 2017-02-03

Initial release
