;;;; Copyright (c) 2017, Philipp Matthias Schäfer (philipp.matthias.schaefer@posteo.de)
;;;;
;;;; This file is part of the Clean Standard Out Test Reporter library.
;;;;
;;;; The Clean Standard Out Test Reporter library is free software: you can
;;;; redistribute it and/or modify it under the terms of the GNU General Public
;;;; License as published by the Free Software Foundation, either version 3 of
;;;; the License, or (at your option) any later version.
;;;;
;;;; The Clean Standard Out Test Reporter library is distributed in the hope
;;;; that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
;;;; warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
;;;; GNU General Public License for more details.
;;;;
;;;; You should have received a copy of the GNU General Public License along
;;;; with the Clean Standard Out Test Reporter library. If not, see
;;;; <https://www.gnu.org/licenses/>.

(defpackage :clean-standard-out-test-reporter/tests/all
  (:use :common-lisp
        :clean-test
        :clean-util))

(in-package :clean-standard-out-test-reporter/tests/all)

(defmacro with-test-test-suite ((name &key parent) &body body)
  `(progn
     (define-test-suite (,name :parent ,parent))
     (unwind-protect
          (progn ,@body)
       (delete-test-suite ,name))))

(defmacro with-suite-output ((var test-suite) &body body)
  `(let ((,var (with-output-to-string (*standard-output*)
                 (run-test-suite ,test-suite))))
     ,@body))

(with-test-suite (clean-standard-out-test-reporter)

  (define-test-case (on-test-suite-*-message)
    (with-test-test-suite (:test-test-suite)
      (with-suite-output (output :test-test-suite)
        (assert-that (string= output "
Run tests for TEST-TEST-SUITE

0 Successes, 0 Failures, 0 Ignored Tests, 0 Errors, 0 Compiler Errors (0 in total)
")))))

  (define-test-case (on-test-suite-*-indentation)
    (with-test-test-suite (:outer-test-suite)
      (with-test-test-suite (:first-inner-test-suite :parent :outer-test-suite)
        (with-test-test-suite (:second-inner-test-suite :parent :outer-test-suite)
          (with-suite-output (output :outer-test-suite)
            (assert-that (string= output "
Run tests for OUTER-TEST-SUITE

  Run tests for FIRST-INNER-TEST-SUITE

  Run tests for SECOND-INNER-TEST-SUITE

0 Successes, 0 Failures, 0 Ignored Tests, 0 Errors, 0 Compiler Errors (0 in total)
")))))))

  (define-test-case (on-test-case-*-ignored)
    (with-test-test-suite(:test-test-suite)
      (define-test-case (:test-test-case :test-suite :test-test-suite :ignore t))
      (with-suite-output (output :test-test-suite)
        (assert-that (string= output "
Run tests for TEST-TEST-SUITE
  TEST-TEST-CASE: Ignored

0 Successes, 0 Failures, 1 Ignored Test, 0 Errors, 0 Compiler Errors (1 in total)
")))))

  (define-test-case (on-test-case-*-success)
    (with-test-test-suite(:test-test-suite)
    (define-test-case (:test-test-case :test-suite :test-test-suite))
    (with-suite-output (output :test-test-suite)
      (assert-that (string= output "
Run tests for TEST-TEST-SUITE
  TEST-TEST-CASE: Success

1 Success, 0 Failures, 0 Ignored Tests, 0 Errors, 0 Compiler Errors (1 in total)
")))))

  (define-test-case (on-test-case-*-error)
    (with-test-test-suite(:test-test-suite)
      (define-test-case (:test-test-case :test-suite :test-test-suite)
        (error "Error!!!"))
      (with-suite-output (output :test-test-suite)
        (assert-that (string= output "
Run tests for TEST-TEST-SUITE
  TEST-TEST-CASE: Error! (Condition: Error!!!)

0 Successes, 0 Failures, 0 Ignored Tests, 1 Error, 0 Compiler Errors (1 in total)
")))))

  (define-test-case (on-test-case-*-compilation-error)
    (with-test-test-suite(:test-test-suite)
      (define-test-case (:test-test-case :test-suite :test-test-suite)
        (+ ""))
      (with-suite-output (output :test-test-suite)
        (assert-that (string= output "
Run tests for TEST-TEST-SUITE
  TEST-TEST-CASE: Compilation Error!

0 Successes, 0 Failures, 0 Ignored Tests, 0 Errors, 1 Compiler Error (1 in total)
")))))

  (define-test-case (on-test-case-*-failure)
    (with-test-test-suite(:test-test-suite)
      (define-test-case (:test-test-case :test-suite :test-test-suite)
        (assert-that (eq t nil)))
      (with-suite-output (output :test-test-suite)
        (assert-that (string= output "
Run tests for TEST-TEST-SUITE
  TEST-TEST-CASE: Failure! (Failed form: (EQ T NIL))

0 Successes, 1 Failure, 0 Ignored Tests, 0 Errors, 0 Compiler Errors (1 in total)
"))))))
